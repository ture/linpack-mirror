import time
import scipy.linalg

import numpy as np

def main():
    while True:
        print("Enter array size (q to quit) [200]:  ")
        try:
            buf = input()
        except EOFError:
            buf = 'q'

        if not buf or buf.isspace():
            arsize = 200
        elif buf[0]=='q' or buf[0]=='Q':
            break
        else:
            arsize = int(buf)

        arsize //= 2
        arsize *= 2

        if arsize < 10:
            print("Too small.")
            continue

        print("\n\nLINPACK benchmark, NumPy/SciPy version, DOUBLE precision.")
        # print("Machine precision: {} digits.".format(BASE10DIG))
        print("Array size {}x{}".format(arsize, arsize))
        print("    Reps Time(s) DGEFA   DGESL  OVERHEAD    KFLOPS")
        print("----------------------------------------------------")

        nreps = 1
        while True:
            if linpack(nreps, arsize) >= 10: break
            nreps *= 2

second = time.process_time

def linpack(nreps, arsize):
    n = arsize // 2
    ops = (2 * n * n * n) / 3.0 + 2 * n * n

    tdgesl = 0
    tdgefa = 0
    totalt = second()

    a, b = matgen(n)

    for i in range(nreps):
        t1 = second()
        lupiv = scipy.linalg.lu_factor(a)
        tdgefa += second() - t1
        t1 = second()
        scipy.linalg.lu_solve(lupiv, b)
        tdgesl += second() - t1

    totalt = second() - totalt

    if totalt < 0.5 or tdgefa + tdgesl < 0.2: return 0

    kflops = nreps * ops / (1000.0 * (tdgefa + tdgesl))

    toverhead = totalt - tdgefa - tdgesl

    if tdgefa < 0: tdgefa = 0
    if tdgesl < 0:  tdgesl = 0
    if toverhead < 0: toverhead = 0

    print("%8d %6.2f %6.2f%% %6.2f%% %6.2f%%  %9.3f"
          % (nreps, totalt, 100 * tdgefa / totalt,
              100 * tdgesl / totalt, 100 * toverhead / totalt,
              kflops))

    return totalt


def matgen(n):
    init = 1325

    a = np.zeros([n, n])

    for j in range(n):
        for i in range(n):
            init = 3125 * init % 65536
            a[j, i] = (init - 32768.0)/16384.0

    b = np.zeros([n, 1])
    for j in range(0, n):
        for i in range(0, n):
            b[i] = b[i] + a[j, i];

    return a, b

if __name__ == '__main__':
    main()
