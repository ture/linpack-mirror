/*
**
** LINPACK.C        Linpack benchmark, calculates FLOPS.
**                  (FLoating Point Operations Per Second)
**
** Translated to C by Bonnie Toy 5/88
**
** Modified by Will Menninger, 10/93, with these features:
**  (modified on 2/25/94  to fix a problem with daxpy  for
**   unequal increments or equal increments not equal to 1.
**     Jack Dongarra)
**
** - Defaults to double precision.
** - User selectable array sizes.
** - Automatically does enough repetitions to take at least 10 CPU seconds.
** - Prints machine precision.
** - ANSI prototyping.
**
** To compile:  cc -O -o linpack linpack.c -lm
**
**
*/

#include <Accelerate/Accelerate.h>

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <float.h>
#include <stdint.h>

#if defined(LPSINGLE)
#define ZERO        0.0
#define ONE         1.0
#define PREC        "Single"
#define BASE10DIG   FLT_DIG

typedef float   REAL;

#elif defined(LPDOUBLE)

#define ZERO        0.0e0
#define ONE         1.0e0
#define PREC        "Double"
#define BASE10DIG   DBL_DIG

typedef double  REAL;

#endif

typedef int32_t LPLONG;
#define PLF PRIi32

static REAL linpack  (LPLONG nreps,int arsize);
static void matgen   (REAL *a,int lda,int n,REAL *b,REAL *norma);

static void *mempool;

int
main(void)
{
    char    buf[80];
    LPLONG  nreps;
    size_t  arsize2d, arsize, malloc_arg, memreq;

    while (1)
    {
        printf("Enter array size (q to quit) [200]:  ");
        fgets(buf,79,stdin);
        if (feof(stdin) || buf[0]=='q' || buf[0]=='Q')
            break;
        if (buf[0]=='\0' || buf[0]=='\n')
            arsize=200;
        else
            arsize=atoi(buf);
        arsize/=2;
        arsize*=2;
        if (arsize<10)
        {
            printf("Too small.\n");
            continue;
        }
        arsize2d = arsize * arsize;
        
        memreq = (arsize2d * sizeof(REAL) 
                  + arsize * sizeof(REAL)
                  + arsize * sizeof(int));

        printf("Memory required:  %"PRIi64"K.\n", 
               (int64_t)(memreq + 512L) >> 10);

        malloc_arg = memreq;
        if (malloc_arg != memreq || (mempool = malloc(malloc_arg)) == NULL)
        {
            printf("Not enough memory available for given array size.\n\n");
            continue;
        }

        printf("\n\nLINPACK benchmark, accelerate version, %s precision.\n",PREC);
        printf("Machine precision:  %d digits.\n",BASE10DIG);
        printf("Array size %"PRIi64" X %"PRIi64".\n",
               (int64_t)arsize, (int64_t)arsize);
        printf("    Reps Time(s) DGEFA   DGESL  OVERHEAD    KFLOPS\n");
        printf("----------------------------------------------------\n");
        nreps=1;
        while (linpack(nreps,arsize)<10.)
            nreps*=2;
        free(mempool);
        printf("\n");
    }
    return 0;
}



/*
**
** DGEFA benchmark
**
** We would like to declare a[][lda], but c does not allow it.  In this
** function, references to a[i][j] are written a[lda*i+j].
**
**   dgefa factors a double precision matrix by gaussian elimination.
**
**   dgefa is usually called by dgeco, but it can be called
**   directly with a saving in time if  rcond  is not needed.
**   (time for dgeco) = (1 + 9/n)*(time for dgefa) .
**
**   on entry
**
**      a       REAL precision[n][lda]
**              the matrix to be factored.
**
**      lda     integer
**              the leading dimension of the array  a .
**
**      n       integer
**              the order of the matrix  a .
**
**   on return
**
**      a       an upper triangular matrix and the multipliers
**              which were used to obtain it.
**              the factorization can be written  a = l*u  where
**              l  is a product of permutation and unit lower
**              triangular matrices and  u  is upper triangular.
**
**      ipvt    integer[n]
**              an integer vector of pivot indices.
**
**      info    integer
**              = 0  normal value.
**              = k  if  u[k][k] .eq. 0.0 .  this is not an error
**                   condition for this subroutine, but it does
**                   indicate that dgesl or dgedi will divide by zero
**                   if called.  use  rcond  in dgeco for a reliable
**                   indication of singularity.
**
**   linpack. this version dated 08/14/78 .
**   cleve moler, university of New Mexico, argonne national lab.
**
**   functions
**
**   blas daxpy,dscal,idamax
**
*/
static void dgefa(REAL *a, LPLONG lda, LPLONG n, LPLONG *ipvt, LPLONG *info)
{
#if defined(LPDOUBLE)
    dgetrf_(&n, &n, a, &lda, ipvt, info);
#elif defined(LPSINGLE)
    sgetrf_(&n, &n, a, &lda, ipvt, info);
#endif
}

/*
**
** DGESL benchmark
**
** We would like to declare a[][lda], but c does not allow it.  In this
** function, references to a[i][j] are written a[lda*i+j].
**
**   dgesl solves the double precision system
**   a * x = b  or  trans(a) * x = b
**   using the factors computed by dgeco or dgefa.
**
**   on entry
**
**      a       double precision[n][lda]
**              the output from dgeco or dgefa.
**
**      lda     integer
**              the leading dimension of the array  a .
**
**      n       integer
**              the order of the matrix  a .
**
**      ipvt    integer[n]
**              the pivot vector from dgeco or dgefa.
**
**      b       double precision[n]
**              the right hand side vector.
**
**      job     integer
**              = 0         to solve  a*x = b ,
**              = nonzero   to solve  trans(a)*x = b  where
**                          trans(a)  is the transpose.
**
**  on return
**
**      b       the solution vector  x .
**
**   error condition
**
**      a division by zero will occur if the input factor contains a
**      zero on the diagonal.  technically this indicates singularity
**      but it is often caused by improper arguments or improper
**      setting of lda .  it will not occur if the subroutines are
**      called correctly and if dgeco has set rcond .gt. 0.0
**      or dgefa has set info .eq. 0 .
**
**   to compute  inverse(a) * c  where  c  is a matrix
**   with  p  columns
**         dgeco(a,lda,n,ipvt,rcond,z)
**         if (!rcond is too small){
**              for (j=0,j<p,j++)
**                      dgesl(a,lda,n,ipvt,c[j][0],0);
**         }
**
**   linpack. this version dated 08/14/78 .
**   cleve moler, university of new mexico, argonne national lab.
**
**   functions
**
**   blas daxpy,ddot
*/
static void
dgesl(REAL *a, LPLONG lda, LPLONG n, LPLONG *ipvt, REAL *b, int job)
{
    LPLONG info;
    LPLONG nrhs = 1;
    LPLONG ldb = n;
    static char trans = 'N';
#if defined(LPDOUBLE)
    dgetrs_(&trans, &n, &nrhs, a, &lda, ipvt, b, &ldb, &info);
#elif defined(LPSINGLE)
    sgetrs_(&trans, &n, &nrhs, a, &lda, ipvt, b, &ldb, &info);
#endif
}


static double
second(void)
{
    return (double)clock() / CLOCKS_PER_SEC;
}

static REAL
linpack(LPLONG nreps, int arsize)
{
    REAL  *a, *b;
    REAL   norma, t1, kflops, tdgesl, tdgefa, totalt, toverhead, ops;
    LPLONG   *ipvt,n,info,lda;
    LPLONG   i,arsize2d;

    lda = arsize;
    n = arsize/2;
    arsize2d = (LPLONG)arsize*(LPLONG)arsize;
    ops=((2.0*n*n*n)/3.0+2.0*n*n);
    a=(REAL *)mempool;
    b=a+arsize2d;
    ipvt=(LPLONG *)&b[arsize];
    tdgesl=0;
    tdgefa=0;
    totalt=second();

    for (i = 0; i < nreps; i++)
    {
        matgen(a, lda, n, b, &norma);
        t1 = second();
        dgefa(a, lda, n, ipvt, &info);
        tdgefa += second() - t1;
        t1 = second();
        dgesl(a, lda, n, ipvt, b, 0);
        tdgesl += second() - t1;
    }
    totalt=second()-totalt;
    if (totalt<0.5 || tdgefa+tdgesl<0.2)
        return(0.);

    kflops = nreps * ops / (1000. * (tdgefa + tdgesl));

    toverhead=totalt-tdgefa-tdgesl;

    if (tdgefa<0.)
        tdgefa=0.;
    if (tdgesl<0.)
        tdgesl=0.;
    if (toverhead<0.)
        toverhead=0.;

    printf("%8"PLF" %6.2f %6.2f%% %6.2f%% %6.2f%%  %9.3f\n",
           nreps, totalt, 100.*tdgefa/totalt,
           100.*tdgesl/totalt, 100.*toverhead/totalt,
           kflops);
    return(totalt);
}


/*
** For matgen,
** We would like to declare a[][lda], but c does not allow it.  In this
** function, references to a[i][j] are written a[lda*i+j].
*/
static void matgen(REAL *a,int lda,int n,REAL *b,REAL *norma)

{
    int init,i,j;

    init = 1325;
    *norma = 0.0;
    for (j = 0; j < n; j++)
        for (i = 0; i < n; i++)
        {
            init = (int)((LPLONG)3125*(LPLONG)init % 65536L);
            a[lda*j+i] = (init - 32768.0)/16384.0;
            *norma = (a[lda*j+i] > *norma) ? a[lda*j+i] : *norma;
        }
    for (i = 0; i < n; i++)
        b[i] = 0.0;
    for (j = 0; j < n; j++)
        for (i = 0; i < n; i++)
            b[i] = b[i] + a[lda*j+i];
}
